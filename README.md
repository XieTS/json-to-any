


# json-to-any

<img alt="" src="https://img.shields.io/badge/Language-Typescript-brightgreen" />

> 支持json转换任意代码格式，支持层级嵌套生成

## 安装
```cmd
npm install json-to-any --save
```

## 使用
> 参考 [在线演示项目](http://xiets.gitee.io/json-to-any-web) 
```ts
import { parse } from "json-to-any";

// 支持js对象
// 支持json数据
const jsonCode = {
  name: "Jack",
  age: 20,
  car: {
    brand: "BMW",
  }
};

const options = {
  original: false, // 是否保留原始数据
};
const entityList = parse(jsonCode, options);

```
### Options
| name     | type    | default | desc                                                            |
|----------|---------|---------|-----------------------------------------------------------------|
| original | boolean | false   | 是否保留原始结构<br/>true：不做任何处理<br/>false：过滤掉所有name相同，key+value类型相同的实体 |
