import { AllVariableType } from "./variable-type";
import { Entity } from "./entity";

export interface Property {
  key: string;
  type: AllVariableType | PropertyType[];
  nullable?: boolean;
  arrayExtend?: ArrayExtend;
  objectExtend?: ObjectExtend;
}

export interface PropertyType {
  type: AllVariableType | PropertyType[];
  reference?: Entity;
}

export interface ArrayExtend {
  types: PropertyType[];
  // 引用了哪些实体，用于生成关系图
}

export interface ObjectExtend {
  // 引用了哪些实体，用于生成关系图
  reference?: Entity;
}
