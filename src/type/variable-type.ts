export type VariableType = "string" | "object" | "number" | "boolean" | "array" | "null" | "undefined";
export type AllVariableType = VariableType | "any";
