import { Property } from "./property";

export interface Entity {
  name: string;
  hash: string;
  properties: Property[];
}

