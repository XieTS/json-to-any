import { parseSource } from "./utils/transfer-json";
import { Entity } from "./type/entity";
import { getPropertyType } from "./utils/properties";
import { ParseOptions, uniqueArray } from "./utils/index";
import { Property } from "./type/property";
import { VariableType } from "./type/variable-type";

export function parse(source: AnyObject | string, options?: ParseOptions): Entity[] {
  let target = parseSource(source);
  let { entityList } = parseObjectToEntityList(target, [], { key: "root" });
  if (options?.original) {
    return entityList;
  }
  // 过滤掉重复的实体
  const hashList = entityList.map(item => item.hash);
  entityList = entityList.filter((item, index) => hashList.indexOf(item.hash) === index);
  return entityList.reverse();
}

function parseObjectToEntityList(source: AnyObject | AnyObject[], entityList: Entity[], options: { key: string }) {
  const entity: Entity = { name: options.key, properties: [], hash: "" };
  // 数组处理
  if (Array.isArray(source)) {
    const isObjectArray = source.every(item => getPropertyType(item).type === "object");
    // 数组对象，有很多联合类型的情况，需要将所有的字段都合并到一个实体中
    if (isObjectArray) {
      // 对象类型
      const allKeys: string[] = uniqueArray([...source.map((item: any) => Object.keys(item)).flat()]);
      allKeys.forEach(key => {
        // 拿到所有类型并去重
        const itemKeyTypes: VariableType[] = uniqueArray(source.map((item: any) => getPropertyType(item[key]).type));
        let property: Property;
        if (itemKeyTypes.some(type => type === "object")) {
          // TODO 处理对象类型
          const { entity: refEntity } = parseObjectToEntityList(source.filter((item: any) => getPropertyType(item[key]).type === "object").map((item: any) => item[key]), entityList, { key });
          property = {
            key,
            type: "object",
            objectExtend: { reference: refEntity }
          };
        } else {
          property = {
            key, type: itemKeyTypes.map(item => ({ type: item })),
            nullable: itemKeyTypes.includes("undefined")
          };
        }
        entity.properties.push(property);
      });
    }
  } else {
    Object.keys(source).forEach((key) => {
      const value = source[key];
      const { type } = getPropertyType(value);
      const handleMap = {
        "object": () => {
          const { entity: refEntity } = parseObjectToEntityList(value, entityList, { key });
          entity.properties.push({ key, type, objectExtend: { reference: refEntity } });
        },
        "array": () => {
          // 如果是对象类型，需要遍历数组中的每一个对象，将所有字段都合并到一个实体中，合并的字段都需要设为可为空
          const firstType = getPropertyType(value[0]).type;
          const allTypes: VariableType[] = value.map((item: any) => getPropertyType(item).type);
          const sameType = allTypes.every(type => type === firstType);
          // 普通数据类型
          if (!allTypes.some(type => type === "object" || type === "array")) {
            const property: Property = { key, type };
            if (sameType) {
              property.arrayExtend = { types: [{ type: firstType }] };
            } else {
              const types = Array.from(new Set(allTypes));
              property.arrayExtend = { types: types.map((item: any) => ({ type: item })) };
            }
            entity.properties.push(property);
            return;
          }
          if (allTypes.every(type => type === "object")) {
            const { entity: refEntity } = parseObjectToEntityList(value, entityList, { key });
            entity.properties.push({ key, type, arrayExtend: { types: [{ type: "object", reference: refEntity }] } });
          } else {
            entity.properties.push({ key, type, arrayExtend: { types: [{ type: "any" }] } });
          }
        }
      };
      if (type in handleMap) {
        handleMap[type as keyof typeof handleMap]();
      } else {
        entity.properties.push({ key, type });
      }
    });
  }
  // 生成哈希值
  entity.hash = generatorHash(entity);
  entityList.push(entity);
  return {
    entity,
    entityList
  };
}

function generatorHash(entity: Entity) {
  return entity.name + "@" + [...entity.properties].sort((a, b) => a.key.localeCompare(b.key)).reduce((pre, cur) => pre + cur.key + cur.type.slice(0, 2) + "_", "");
}

