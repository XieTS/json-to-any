export interface ParseOptions {
  // 原始数据，不做任何多余的处理
  original: boolean;
}

export function uniqueArray<T>(arr: T[]): T[] {
  return Array.from(new Set(arr));
}
