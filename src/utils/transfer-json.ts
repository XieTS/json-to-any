import { isString } from "./shared";

export function parseSource(source: AnyObject | string): AnyObject {
  let result = source;
  try {
    if (isString(source)) {
      result = JSON.parse(source);
    }
  } catch (e) {
    throw Error("parse code error ==>" + e);
  }
  return result as AnyObject;
}

