import { VariableType } from "../type/variable-type";
import { isArray, isObject } from "./shared";

export function getPropertyType(rawValue: any): {
  type: VariableType
} {
  if (rawValue === null) {
    return { type: "null" };
  }
  if (isArray(rawValue)) {
    return { type: "array" };
  }
  if (isObject(rawValue)) {
    return { type: "object" };
  }
  return { type: typeof rawValue as VariableType };
}

