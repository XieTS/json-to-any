const esbuild = require("esbuild");
const args = process.argv.slice(2);

async function start() {
  const options = {
    outdir: "/dist",
    entryPoints: ["./src/index.ts"],
    bundle: true,
    platform: "node",
    target: ["esnext"]
  };
  if (args.includes("--watch")) {
    const ctx = await esbuild.context(options);
    ctx.watch();
  } else {
    await esbuild.build(options);
  }
}

start();
