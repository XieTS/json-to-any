import * as fs from "fs";
import { parseSource } from "../src/utils/transfer-json";

describe("index", () => {
  let content = fs.readFileSync("./tests/easy.json", "utf-8");
  test("parse object", () => {
    // 获取json文件内容
    const result = parseSource(JSON.parse(content));
    expect(result).toBeInstanceOf(Object);
  });
  test("parse json string", () => {
    const result = parseSource(content);
    expect(result).toBeInstanceOf(Object);
  });
});
